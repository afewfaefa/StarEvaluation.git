package com.qhzz.starevaluation;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private StarView star;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        star = ((StarView) findViewById(R.id.main_star));
        star.setOnLevelChangeListener(new StarView.OnLevelChangeListener() {
            @Override
            public void levelChange(int level) {
                Toast.makeText(MainActivity.this, "当前星级：" + level, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
