package com.qhzz.starevaluation;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

public class StarView extends View {

    public StarView(Context context) {
        this(context, null);
    }

    public StarView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }


    private Paint mPaint;
    private int mWidth, mHeight;
    private float starHeight, starWidth;    // 星星的高
    private int starCount = 8; // 星星数量
    private int level = 0;   // 星级
    private float spacing = 500;  // 星星之间的间距
    private float leftSpacing = 200;  // 左右上下间距
    private float rightSpacing = 200;
    private float topSpacing = 2;
    private float bottomSpacing = 2;

    private Bitmap star, starPressed; // 两种状态的星星图片

    private OnLevelChangeListener onLevelChangeListener;

    public void setOnLevelChangeListener(OnLevelChangeListener onLevelChangeListener) {
        this.onLevelChangeListener = onLevelChangeListener;
    }

    private void init() {
        mPaint = new Paint();
        star = BitmapFactory.decodeResource(getResources(), R.drawable.icon_evaluate_star);
        starPressed = BitmapFactory.decodeResource(getResources(), R.drawable.icon_evaluate_star_pressed);
        starWidth = star.getWidth();
        starHeight = star.getHeight();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int oldLevel = level;
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN: // 手指按下
                break;
            case MotionEvent.ACTION_MOVE: // 手指滑动
                float x = event.getX();
                float y = event.getY();
                // 当点击区域在  星星所在区域时 才点击有效
                if (y > topSpacing && y < topSpacing + starHeight) {
                    // 根据点击位置确实星级
                    if (x < leftSpacing ) {
                        // 小于左边距 表示没有点到一个星星
                        level = 0;
                    } else {
                        // 只要左边距肯定已经点到星星了  除于星星宽个间距即知道点击了那个星星
                        level = (int) ((x - leftSpacing) / (starWidth + spacing)) + 1;
                        Log.e("AAA—>", "onTouchEvent: " + level );
                        if (oldLevel != level) {
                            // 只有当星级发生改变时才去刷新布局 不做没必要刷新
                            if (onLevelChangeListener != null) {
                                onLevelChangeListener.levelChange(level);
                            }
                            postInvalidate();
                        }
                    }
                }

                break;
            case MotionEvent.ACTION_UP: // 手指抬起
                break;
        }

        return true;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        mWidth = MeasureSpec.getSize(widthMeasureSpec);
        //
        // 实际所需要的宽 = 星星的宽 * 星星数量 + 星星之间的间距 * 间距数  + 左右间距
        float totalWidthSpacing = (starCount - 1) * spacing + leftSpacing + rightSpacing; // 总的间距
        float width = starWidth * starCount + totalWidthSpacing;
        switch (widthMode) {
            case MeasureSpec.AT_MOST:
            case MeasureSpec.EXACTLY:
                // 当实际所需的宽 大于控件所设定的宽时   应该按比例缩小实际所需要宽来满足控件所给宽
                if (width > mWidth) {
                    // 计算比例
                    float scale = mWidth / width;
                    starWidth = starWidth * scale;
                    spacing = spacing * scale;
                    leftSpacing = leftSpacing * scale;
                    rightSpacing = rightSpacing * scale;

                } else {

                    // 如果实际所需宽小于 控件所给宽  那就加大左右间距  尽量保持居中效果

                    float diff = width - mWidth;

                    leftSpacing = leftSpacing + diff / 2;

                    rightSpacing = rightSpacing + diff / 2;

                }

                // 重新计算
                totalWidthSpacing = (starCount - 1) * spacing + leftSpacing + rightSpacing; // 总的间距
                width = starWidth * starCount + totalWidthSpacing;
                mWidth = (int) (width + totalWidthSpacing);
                break;
            case MeasureSpec.UNSPECIFIED:
                // 未指定的情况下 我就安实际所需宽高来 做控件宽高

                mWidth = (int) width;

                break;
        }

        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        mHeight = MeasureSpec.getSize(heightMeasureSpec);
        // 实际所需高
        float height = starHeight + topSpacing + bottomSpacing;
        switch (heightMode) {
            case MeasureSpec.AT_MOST:
            case MeasureSpec.EXACTLY:
                // 当控件指定高时  尽可能满足指定的高
                if (height > mHeight) {
                    // 当实际所需高大于指定高时  按比例缩小实际所需高
                    float scale = mHeight / height;
                    starHeight = starHeight * scale;
                    topSpacing = topSpacing * scale;
                    bottomSpacing = bottomSpacing * scale;
                } else {
                    // 实际所需高小于指定高时  将多余的都加到 上下间距
                    float diff = mHeight - height;
                    topSpacing = topSpacing + diff / 2;
                    bottomSpacing = bottomSpacing + diff / 2;

                }
                // 重新计算高
                mHeight = (int) (starHeight + topSpacing + bottomSpacing);
                break;
            case MeasureSpec.UNSPECIFIED:
                // 未指定的情况下 我就安实际所需宽高来 做控件宽高
                mHeight = (int) height;
                break;
        }

        // 设置宽高
        setMeasuredDimension(mWidth, mHeight);


    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mHeight = h;
        mWidth = w;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        for (int i = 0; i < starCount; i++) {
            Bitmap bitmap = star;
            if (i < level) {
                bitmap = starPressed;
            }
            // 表示图片需要绘制区域
            Rect src = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
            // 表示图片应该被绘制在的区域
            RectF dst = new RectF();
            dst.top = topSpacing ;
            dst.left = leftSpacing + (starWidth + spacing) * i;
            dst.right = dst.left + starWidth;
            dst.bottom = dst.top + starWidth;

            canvas.drawBitmap(bitmap, src, dst, mPaint);
        }

    }

    public interface OnLevelChangeListener {
        void levelChange(int level);
    }



}
